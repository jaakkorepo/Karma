const Discord = require('discord.js');
const Commando = require('discord.js-commando');
const sqlite = require('sqlite');
const path = require('path');
const {
	version,
	description,
	name,
	author
} = require('./package.json');
const config = require('./config/config.json');
const {
	calcDays
} = require("./utils/tools/calcdays.js");
const statusList = require("./utils/lists/status.json");


const client = new Commando.Client({
	owner: config.ownerid,
	commandPrefix: config.prefix,
	disableEveryone: true,
	unknownCommandResponse: false
});

exports.shard = new Discord.ShardClientUtil(client);

client.registry
	// Registers your custom command groups
	.registerGroups([
        ['rp_sfw', 'SFW Roleplay'],
        ['pics_sfw', 'SFW Pictures'],
        ['fun', 'Fun'],
        ['jlogs', 'Join Logs']
    ])

	// Registers all built-in groups, commands, and argument types
	.registerDefaults()

	// Registers all of your commands in the ./commands/ directory
	.registerCommandsIn(path.join(__dirname, 'commands'));

client.on('ready', () => {
	console.log(`\nLogged in:
    Bot: ${client.user.tag} / ${client.user.id}
    Shard: ${this.shard.id} (Shards: ${this.shard.count})
    `);

	console.log(`Build info:
    Name: ${name}
    Version: ${version} (${description})
    Author: ${author}
    `);

	const setStatus = () => {
		const interval = 300;
		const refresh = () => {
			status = statusList[Math.floor(Math.random() * statusList.length)];
			client.user.setGame(status);
			console.log(`\nUpdated Shard ${client.shard.id} status: "Playing ${status}" - One every ${interval}s\n`)
		};

		setInterval(refresh, interval * 1000);
		refresh();
	};

	setStatus()

	/*client.user.setUsername('Karma Alpha');
	client.user.setAvatar('./assets/avatar_colours.png')*/

	// Sorry I wanted to update bot info, left it there  ^
});

client.setProvider(
	sqlite.open(
		path.join(__dirname, 'settings.sqlite3')
	).then(
		db => new Commando.SQLiteProvider(db)
	)
).catch(
	console.error
);


// JOIN LOGS FOR SERVERS, CHECK /commands/jlogs

client.on('guildMemberAdd', member => {

	//Let's gather some data.

	const targetChannel = member.guild.channels.find("id", member.guild.settings.get("joinLogsChannel", null));
	const autoRole = member.guild.roles.find("id", member.guild.settings.get("joinLogsAutoRole", null));
	const autoBotRole = member.guild.roles.find("id", member.guild.settings.get("joinLogsBotRole", null));
	const verification = member.guild.settings.get("joinLogsVerification", null);

	if (targetChannel) { // If join logs is enabled (a channel is set)

		// Let's get some messages for it lol
		const titles = [
      `${member.user.username} popped up!`,
      `${member.user.username} joined in the fun!`,
      `${member.user.username} joined.`,
      `${member.user.username} clicked the invite!`
    ];

		// Chooses a random title
		title = titles[Math.floor(Math.random() * titles.length)];

		let embed = new Discord.RichEmbed()

			// Sets base info for the embed
			.setTitle(title)
			.setThumbnail(member.user.avatarURL)
			.setColor("GREEN")
			.setFooter(new Date())

			// Adds fields with info related to the member
			.addField("User", `${member.user.tag} (${member.user})`)
			.addField("ID", member.user.id)
			.addField("Account Made", `${calcDays(new Date(), member.user.createdAt)} days ago`)
			.addField("Member Count", member.guild.memberCount)



		if (member.user.bot && autoBotRole) { // If member is a bot AND auto bot role exists
			targetChannel.send({
				embed
			});
			member.addRole(autoBotRole, "Join Logs (Auto Bot Role)");

		} else if (!member.user.bot && !verification) { // If member is not a bot and there's no verification
			targetChannel.send({
				embed
			});
			if (autoRole) { // If there's an auto role
				member.addRole(autoRole, "Join Logs (Auto Role)");
			};

		} else if (!member.user.bot && verification && autoRole) { // If member is not a bot and there's verification and an autorole
			const welcomeMessage = `${member}, welcome to the server! You're currently unverified. Please wait for a moderator to check you. We'll ping you once we're done.`
			targetChannel.send(welcomeMessage, {
					embed
				})
				.then(function (message) {
					message.react("✅")
					message.react("❌")
				})
				.catch(
					console.error
				);

		}

	}
});

client.on('guildMemberRemove', member => {

	const targetChannel = member.guild.channels.find("id", member.guild.settings.get("joinLogsChannel"));

	if (targetChannel) {

		const titles = [`${member.user.username} left the building.`, `Bye ${member.user.username}!`,
    `See you later, ${member.user.username}.`, `${member.user.username} has disappeared.`];
		title = titles[Math.floor(Math.random() * titles.length)];

		let embed = new Discord.RichEmbed()

			.setTitle(title)
			.setThumbnail(member.user.avatarURL)
			.setColor("RED")
			.setFooter(new Date())

			.addField("User", `${member.user.tag} (${member.user})`)
			.addField("ID", member.user.id)
			.addField("Account Made", `${calcDays(new Date(), member.user.createdAt)} days ago`)
			.addField(`Joined ${member.guild.name}`, `${calcDays(new Date(), member.joinedAt)} days ago`)
			.addField("Member Count", member.guild.memberCount)

		targetChannel.send({
			embed
		});
	}
});

client.login(config.token);
