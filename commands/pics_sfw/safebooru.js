const { Command } = require('discord.js-commando');
const Discord = require('discord.js');
const { safebooru } = require("./../../utils/web/safebooru.js");


module.exports = class ChannelCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'safebooru',
      group: 'pics_sfw',
      memberName: 'safebooru',
      description: 'Searches safebooru for something',
      examples: ['safebooru braixen', "safebooru canine score:>30"],
      guildOnly: true,
      throttling: {
          usages: 2,
          duration: 10
      },
      aliases: ["sb", "safe"],
      args: [
        {
          key: 'search',
          label: 'search',
          prompt: 'What would you like me to search for?',
          type: 'string'
        }
      ]
    });
  }

  async run(msg, args) {

    // Let's create the embed!
    let embed = new Discord.RichEmbed();

    // Start typing
    await msg.channel.startTyping();

    // Fetching a pic from safebooru, utils/web/safebooru.js
    const data = await safebooru(args.search);
    const result = data[0];
    const error = data[1];

    if (!error) { // This gets executed if there are no errors and results are found

      // We need some data bois
      const sourceurl = "https://safebooru.org/index.php?page=post&s=view&id=" + result["id"];
      const fileurl = `https://safebooru.org/images/${result["directory"]}/${result["image"]}`;
      const score = result["score"];
      const resolution = result["width"] + "x" + result["height"];
      const author = msg.author.tag;

      // OK we're ready to modify the embed!

      embed.setTitle(args.search);
      embed.setURL(sourceurl);
      embed.setImage(fileurl);
      embed.setDescription(`Score: ${score} | Resolution: ${resolution}`);
      embed.setFooter(author);
      embed.setColor("BLUE");

    } else { // If there is an error or no results

      // hold my beer
      embed.setTitle("Oops!");
      embed.setDescription(error);
      embed.setColor("RED");

    };

    await msg.channel.stopTyping(true);

    // Sends the embed
    return msg.channel.send({embed});
  }
};
