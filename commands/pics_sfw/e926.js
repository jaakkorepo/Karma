const { Command } = require('discord.js-commando');
const Discord = require('discord.js');
const { e926 } = require("./../../utils/web/e926.js");


module.exports = class ChannelCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'e926',
      group: 'pics_sfw',
      memberName: 'e926',
      description: 'Searches E926 for something',
      examples: ['e926 braixen', "e926 canine score:>30"],
      guildOnly: true,
      throttling: {
          usages: 2,
          duration: 10
      },
      aliases: ["e9"],
      args: [
        {
          key: 'search',
          label: 'search',
          prompt: 'What cute stuff should we search for today?',
          type: 'string'
        }
      ]
    });
  }

  async run(msg, args) {
    // Start typing
    await msg.channel.startTyping();

    // Fetching a pic from e926, utils/web/e926.js
    const data = await e926(args.search);
    const result = data[0];
    const error = data[1];

    // Let's create the embed!
    let embed = new Discord.RichEmbed();

    if (!error) { // This gets executed if there are no errors and results are found

      // We need some data bois
      const sourceurl = "https://e926.net/post/show/" + result["id"];
      const fileurl = result["file_url"];
      const score = result["score"];
      const resolution = result["width"] + "x" + result["height"];
      const rating = result["rating"];
      const artists = result["artist"].join(", ");
      const author = msg.author.tag;

      // OK we're ready to modify the embed!

      embed.setTitle(args.search);
      embed.setURL(sourceurl);
      embed.setImage(fileurl);
      embed.setDescription(`Score: ${score} | Resolution: ${resolution} | Rating: ${rating}\nArtist(s): ${artists}`);
      embed.setFooter(author);
      embed.setColor("BLUE");

    } else { // If there is an error or no results

      // hold my beer
      embed.setTitle("Oops!");
      embed.setDescription(error);
      embed.setColor("RED");

    };


    // Sends the embed
    await msg.channel.stopTyping(true);
    return msg.channel.send({embed});
  }
};
