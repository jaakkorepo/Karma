const { Command } = require('discord.js-commando');
const Discord = require('discord.js');
const { shibe } = require("./../../utils/web/shibe.js");


module.exports = class ChannelCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'shibe',
      group: 'pics_sfw',
      memberName: 'shibe',
      description: 'Doge is still funny right lol',
      examples: ['shibe', "shibe arguments are useless sorry"],
      guildOnly: true,
      aliases: [
        "shib",
        "doge"
      ],
      throttling: {
          usages: 2,
          duration: 10
      }
    });
  }

  async run(msg, args) {

    // Start typing
    await msg.channel.startTyping();

    // Fetching a pic from shibe.online, utils/web/shibe.js
    const pic = await shibe();
    let embed = new Discord.RichEmbed()

      // Sets info for embed
      .setImage(pic[0])
      .setTitle(pic[1])
      .setColor(msg.guild.me.displayColor)

    // Sends the embed
    msg.channel.stopTyping(true);
    return msg.channel.send({embed});
  }
};
