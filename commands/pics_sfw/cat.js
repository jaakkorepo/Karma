const { Command } = require('discord.js-commando');
const Discord = require('discord.js');
const { cat } = require("./../../utils/web/meow.js");


module.exports = class ChannelCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'cat',
      group: 'pics_sfw',
      memberName: 'cat',
      description: 'cats are cute, right?',
      examples: ['cat', "cat arguments are useless sorry"],
      guildOnly: true,
      aliases: [
        "meow",
        "kitty",
        "kitten"
      ],
      throttling: {
          usages: 2,
          duration: 10
      }
    });
  }

  async run(msg, args) {

    // Start typing
    await msg.channel.startTyping();

    // Fetching a pic from random.cat, utils/web/meow.js
    const pic = await cat();
    let embed = new Discord.RichEmbed()

      // Sets info for embed
      .setImage(pic[0])
      .setTitle(pic[1])
      .setColor(msg.guild.me.displayColor)

    // Sends the embed
    msg.channel.stopTyping(true);
    return msg.channel.send({embed});
  }
};
