const { Command } = require('discord.js-commando');
const Discord = require('discord.js');
const { bird } = require("./../../utils/web/tweet.js");


module.exports = class ChannelCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'bird',
      group: 'pics_sfw',
      memberName: 'bird',
      description: 'Birds are cute, right?',
      examples: ['bird', "bird arguments are useless sorry"],
      guildOnly: true,
      aliases: [
        "birb",
        "tweet"
      ],
      throttling: {
          usages: 2,
          duration: 10
      }
    });
  }

  async run(msg, args) {

    // Start typing
    await msg.channel.startTyping();

    // Fetching a pic from random.birb.pw, utils/web/tweet.js
    const pic = await bird();
    let embed = new Discord.RichEmbed()

      // Sets info for embed
      .setImage(pic[0])
      .setTitle(pic[1])
      .setColor(msg.guild.me.displayColor)

    // Sends the embed
    msg.channel.stopTyping(true);
    return msg.channel.send({embed});
  }
};
