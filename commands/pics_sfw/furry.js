const { Command } = require('discord.js-commando');
const Discord = require('discord.js');
const { furry } = require("./../../utils/web/furry.js");


module.exports = class ChannelCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'furry',
      group: 'pics_sfw',
      memberName: 'furry',
      description: 'Fursuits are cute I guess',
      examples: ['furry', "furry arguments are useless sorry"],
      guildOnly: true,
      aliases: [
        "fursuit",
        "fur",
        "fluff"
      ],
      throttling: {
          usages: 2,
          duration: 10
      }
    });
  }

  async run(msg, args) {

    // Fetching a pic from a web server you have to set up, utils/web/furry.js
    const pic = await furry();
    let embed = new Discord.RichEmbed()

      // Sets info for embed
      .setImage(pic[0])
      .setTitle(pic[1])
      .setColor(msg.guild.me.displayColor)

    // Sends the embed
    return msg.channel.send({embed});
  }
};
