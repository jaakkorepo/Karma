const { Command } = require('discord.js-commando');
const Discord = require('discord.js');
const { dog } = require("./../../utils/web/woof.js");


module.exports = class ChannelCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'dog',
      group: 'pics_sfw',
      memberName: 'dog',
      description: 'Dogs are cute, right?',
      examples: ['dog', "dog arguments are useless sorry"],
      guildOnly: true,
      aliases: [
        "woof",
        "doggo"
      ],
      throttling: {
          usages: 2,
          duration: 10
      }
    });
  }

  async run(msg, args) {

    // Start typing
    await msg.channel.startTyping();

    // Fetching a pic from random.dog, utils/web/woof.js
    const pic = await dog();
    let embed = new Discord.RichEmbed()

      // Sets info for embed
      .setImage(pic[0])
      .setTitle(pic[1])
      .setColor(msg.guild.me.displayColor)

    // Sends the embed
    msg.channel.stopTyping(true);
    return msg.channel.send({embed});
  }
};
