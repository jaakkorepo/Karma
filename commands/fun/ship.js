const { Command } = require('discord.js-commando');
const Discord = require('discord.js');
const { calcLove, nameShip } = require("./../../utils/tools/calclove.js")


module.exports = class ChannelCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'ship',
      group: 'fun',
      memberName: 'ship',
      description: 'gotta have a ship command right',
      examples: ['ship jaq moikka', "ship @jaq @moikka"],
      guildOnly: true,
      throttling: {
          usages: 2,
          duration: 10
      },
      args: [
        {
          key: 'member1',
          label: 'user',
          prompt: 'Who do you want to ship?',
          type: 'member'
        },
        {
          key: 'member2',
          label: 'user',
          prompt: 'Who would you like to ship them with?',
          type: 'member'
        }
      ]
    });
  }

  async run(msg, args) {


    // Settings shortcuts, don't mind if I do
    const me = this.client.user;
    const user1 = args.member1;
    let user2 = args.member2;
    let desc = "Ships are cute~"

    // No selfcest thanks
    if (user1 === user2) {
      user2 = msg.guild.me; // If user 1 is the same as user 2, the bot will become user 2
      desc = "Don't be sad, I can be with you~ <3" // Replaces the desc
    };

    // Okay mom I'm gonna make an embed

    let embed = new Discord.RichEmbed()

      // Sets info for embed
      .setTitle(`Ship | ${user1.displayName} x ${user2.displayName}`)
      .setDescription(desc)
      .setColor(msg.guild.me.displayColor)

      // Let's add some stuff.
      .addField("User 1", user1.displayName)
      .addField("User 2", user2.displayName)
      .addField("Ship Name", nameShip(user1.displayName, user2.displayName))
      .addField("Ship Score", calcLove(user1.displayName, user2.displayName) + "%")

    // Sends the embed
    return msg.channel.send({embed});
  }
};
