const {Command} = require('discord.js-commando');
const {repository} = require('./../../package.json');

module.exports = class ChannelCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'github',
      group: 'util',
      memberName: 'github',
      description: 'Shows the link to my Github repository',
      examples: ["github", "github there are no arguments smh"],
      guildOnly: false,
      throttling: {
          usages: 2,
          duration: 10
      },
      aliases: [
        "git",
        "repo",
        "source"
      ]
    });
  }

  async run(msg, args) {

    msg.say(`Here's the link to my Github page: ${repository.url}`);

  }
};
