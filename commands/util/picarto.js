const {Command} = require('discord.js-commando');
const Discord = require('discord.js');
const {picarto} = require("./../../utils/web/picarto.js");
const {calcDays} = require("./../../utils/tools/calcdays.js");

module.exports = class ChannelCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'picarto',
      group: 'util',
      memberName: 'picarto',
      description: 'Fetches info about a Picarto streamer',
      examples: ["picarto Senty", "picarto some streamer idk"],
      guildOnly: true,
      throttling: {
          usages: 2,
          duration: 10
      },
      aliases: [
        "picartotv",
        "picart"
      ],
      args: [
        {
          key: 'streamer',
          label: 'streamer',
          prompt: "Who's the streamer you would like to get info about?",
          type: 'string'
        }
      ]
    });
  }

  async run(msg, args) {

    // Let's get the data for the user.
    const picartotv = await picarto(args.streamer);
    const data = picartotv[0];
    const error = picartotv[1];

    // Let's start building an embed!
    let embed = new Discord.RichEmbed()

    if (data) {

      // I'll start gathering data like some random Chinese company.

      const name = data["name"];
      const title = data["title"];
      const category = data["category"];
      const adult = data["adult"];
      const gaming = data["gaming"];
      const account = data["account_type"];
      const currentViewers = data["viewers"];
      const totalViewers = data["viewers_total"];
      const followers = data["followers"];
      const subscribers = data["subscribers"];
      const isPrivate = data["private"];
      const guestchat = data["guest_chat"];
      const online = data["online"];
      const link = "https://picarto.tv/" + data["name"];

      let tags = data["tags"];
      if (data["tags"].length > 0) {
        tags = tags.join(", ");
      } else {
        tags = "None!";
      };

      let lastLive = new Date(data["last_live"].split("T")[0]);
      if (lastLive.getFullYear() < 2000) { // If hasn't streamed, the year is like 1970
        lastLive = "Never!";
      } else {
        lastLive = `${calcDays(new Date(), lastLive)} days ago`;
      }

      //Sets basic info for the embed
      embed.setTitle(`${data["name"]} | Info`);
      embed.setURL(link);
      embed.setDescription(title);
      embed.setThumbnail(data["avatar"]);

      // Nice friendly blue
      embed.setColor("BLUE");

      // Let's start adding some fields.

      embed.addField(
        "Category",
        category,
        true
      );

      embed.addField(
        "Adult",
        adult,
        true
      );

      embed.addField(
        "Gaming",
        gaming,
        true
      );

      embed.addField(
        "Account Type",
        account,
        true
      );

      embed.addField(
        "Viewers / Current",
        currentViewers,
        true
      )

      embed.addField(
        "Viewers / Total",
        totalViewers,
        true
      );

      embed.addField(
        "Followers",
        followers,
        true
      );

      embed.addField(
        "Subscribers",
        subscribers,
        true
      );

      embed.addField(
        "Private",
        isPrivate,
        true
      );

      embed.addField(
        "Guest Chat",
        guestchat,
        true
      );

      embed.addField(
        "Online",
        online,
        true
      );

      embed.addField(
        "Last Live",
        lastLive,
        true
      );

      embed.addField(
        "Tags",
        tags,
        false
      );

    } else { // SO like some error happened

      //Sets basic info for the embed
      embed.setTitle("Something happened?");
      embed.setDescription(error);

      // Not-so-nice not-so-friendly red
      embed.setColor("RED");

    }

    // Finally sends the message
    msg.embed(embed);
  }
};
