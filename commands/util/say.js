const { Command } = require('discord.js-commando');

module.exports = class ChannelCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'say',
      group: 'util',
      memberName: 'say',
      description: 'Makes the bot say something!',
      examples: ["say fuck", "say TO be hoNEST YHou NEED EERY HigH IQ TO UNDerSTAnd RIck AND moRTY!"],
      guildOnly: true,
      throttling: {
          usages: 2,
          duration: 10
      },

      args: [
        {
          key: 'text',
          label: 'text',
          prompt: 'What should I say?',
          type: 'string'
        }
      ]
    });
  }

  async run(msg, args) {
    msg.delete()
    msg.say(args.text)
  }
};
