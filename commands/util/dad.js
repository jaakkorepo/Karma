const {Command} = require('discord.js-commando');
const Discord = require('discord.js');
const {dad} = require("./../../utils/web/dad.js");

module.exports = class ChannelCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'dad',
      group: 'util',
      memberName: 'dad',
      description: 'Everyone loves dad jokes',
      examples: ["dad", "dad hi im useless"],
      guildOnly: true,
      throttling: {
          usages: 2,
          duration: 10
      },
      aliases: [
        "dadjoke",
        "pun",
        "pain"
      ]
    });
  }

  async run(msg, args) {

    // Let's get the dad joke lol
    const dadJoke = await dad();

    // Let's start building an embed!

    let embed = new Discord.RichEmbed()

      // Sets basic info for the embed
      .setTitle("Dad Joke")
      .setDescription(dadJoke)

      // Sets the colour to the bots own roles colour
      .setColor(msg.guild.me.displayColor)

    // Finally sends the message
    msg.embed(embed);
  }
};
