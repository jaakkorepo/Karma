const { Command } = require('discord.js-commando');
const Discord = require('discord.js');

module.exports = class ChannelCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'roulette',
      group: 'util',
      memberName: 'roulette',
      description: 'Uhhh picks a random member',
      examples: ["roulette fuck me", "roulette"],
      guildOnly: true,
      throttling: {
          usages: 2,
          duration: 5
      },

      args: [
        {
          key: 'text',
          label: 'text',
          prompt: 'What\'s the prize?',
          type: 'string',
          default: ""
        }
      ]
    });
  }

  async run(msg, args) {
    const member = msg.guild.members.random();

    let desc;
    let avatar;

    if (args.text) {
      desc = `The winner of \`${args.text}\` is ${member.displayName}.\n(<@${member.user.id}>)`;
    } else {
      desc = `The winner is ${member.displayName}.\n(<@${member.user.id}>)`;
    };

    let embed = new Discord.RichEmbed()

      // Sets info for embed
      .setTitle(`Roulette by ${msg.member.displayName}`)
      .setDescription(desc)
      .setThumbnail(member.user.displayAvatarURL)
      .setColor(msg.guild.me.displayColor)

    // Sends the embed
    return msg.channel.send({embed});
  }
};
