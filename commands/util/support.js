const {Command} = require('discord.js-commando');
const {supportUrl} = require("./../../config/config.json");
const {repository} = require('./../../package.json');

module.exports = class ChannelCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'support',
      group: 'util',
      memberName: 'support',
      description: 'Do you need help with something?',
      examples: ["support", "support there are no arguments smh"],
      guildOnly: false,
      throttling: {
          usages: 2,
          duration: 10
      }
    });
  }

  async run(msg, args) {

    msg.say(`Here's the link to get support: ${supportUrl}
Alternatively, you can use <${repository.url}/issues>`);

  }
};
